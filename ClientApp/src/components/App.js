import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './Layout';
import { Home } from './Home';
import { About } from './About';
import { Lazypointer } from './Lazypointer';
import { LazypointerEditor } from './LazypointerEditor';
import { Boxfer } from './Boxfer';
import { Wordfrag } from './Wordfrag';
import { Starryblocks } from './Starryblocks';
import { Mario } from './Mario';
import { Minecraft } from './Minecraft';
import { Wizard } from './Wizard';
import { Greeny } from './Greeny';
import { Current } from './Current';
import { Website } from './Website';
import { Fonstr } from './Fonstr';

import '../css/custom.css'


export default class App extends Component {
  static displayName = App.name;

  render () {
    return (
        <Layout>
            <Route exact path='/' component={Home} />
            <Route path='/about' component={About} />

            <Route path='/games/lazypointer' component={Lazypointer} />
            <Route path='/games/boxfer' component={Boxfer} />
            <Route path='/games/wordfrag' component={Wordfrag} />
            <Route path='/games/starryblocks' component={Starryblocks} />
            <Route path='/games/mario' component={Mario} />
            <Route path='/games/minecraft' component={Minecraft} />
            <Route path='/games/wizard' component={Wizard} />
            <Route path='/games/greeny' component={Greeny} />
            <Route path='/games/current' component={Current} />

            <Route path='/web/website' component={Website} />

            <Route path='/apps/lazypointer-editor' component={LazypointerEditor} />
            <Route path='/apps/fonstr' component={Fonstr} />
            
        </Layout>
    );
  }
}
