﻿import React, { Component } from 'react';

export class Gif extends Component {
    static displayName = Comment.name;

    constructor(props) {
        super(props);

        this.state = {
            id: props.id,
            title: props.title,
            url: props.url,
            preview: props.preview,
        };
    }

    render() {

        return (
            <div className="card" style={{ width: '18rem' }}>
                <img src={this.state.preview} className="card-img-top" alt="..."></img>
                <div className="card-body">
                    <h5 className="card-title">{this.state.title}</h5>
                    <a href={this.state.url} className="btn btn-primary">Show full</a>
                </div>
            </div>
        );
    }

}
