﻿import React, { Component } from 'react';
import '../../node_modules/react-vis/dist/style.css';
import { FlexibleXYPlot, HorizontalBarSeries, XAxis, YAxis } from 'react-vis';

export class ExperienceBarGraph extends Component {
    static displayName = ExperienceBarGraph.name;

    constructor(props) {
        super(props);

        this.state = {
            data: props.data,
            margin: props.margin
        };

        this.sortAndColorizeData(this.state.data);
    }


    compare(a, b) {
        if (a.x < b.x) {
            return -1;
        }
        if (a.x > b.x) {
            return 1;
        }
        return 0;
    }

    sortAndColorizeData(data) {

        // Sort, longest experience on top
        data.sort(this.compare);

        // Decrease strength of color 
        var colorStep = 1.0 / data.length;
        var currentStep = 0.0;
        var arrayLength = data.length;
        for (var i = 0; i < arrayLength; i++) {
            data[i].color -= currentStep;
            currentStep += colorStep;
        }
    }

    render() {

        return (
            <div>
                <FlexibleXYPlot height={40 * this.state.data.length} yType="ordinal" margin={{ left: this.state.margin, right: this.state.margin }}>
                    <XAxis tickFormat={val => Math.round(val) === val ? val : ""}/>
                    <YAxis/>
                    <HorizontalBarSeries data={this.state.data} />
                </FlexibleXYPlot>
                <p className="expYearsText">Years of experience</p>
            </div>
        );
    }


}
