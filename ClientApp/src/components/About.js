﻿import React, { Component } from 'react';
import { ExperienceBarGraph } from './ExperienceBarGraph';

export class About extends Component {
    static displayName = About.name;

    constructor(props) {
        super(props);

        this.state = {

        };
    }

    render() {

        /*
            X: YEARS
            Y: TEXT
            COLOR: 1
         */

        const languageData = [
            { x: 8, y: 'C#', color: "1" },
            { x: 7, y: 'C++', color: "1" },
            { x: 2, y: 'Java', color: "1" },
            { x: 3, y: 'JavaScript', color: "1" },
            { x: 7, y: 'SQL', color: "1" },
            { x: 3, y: 'HTML', color: "1" },
            { x: 3, y: 'CSS', color: "1" },
            { x: 2, y: 'PHP', color: "1" },
            { x: 0.5, y: 'Visual Basic', color: "1" },
            { x: 0.5, y: 'Pascal', color: "1" },
        ];

        const frameworkData = [
            { x: 8, y: '.NET', color: "1" },
            { x: 1, y: 'ASP.NET', color: "1" },
            { x: 5, y: 'WPF', color: "1" },
            { x: 5, y: 'WinForms', color: "1" },
            { x: 5, y: 'MFC', color: "1" },
            { x: 1, y: 'Entity Framework', color: "1" },
            { x: 1, y: 'React', color: "1" },
        ];

        const databaseData = [
            { x: 8, y: 'MsSQL', color: "1" },
            { x: 5, y: 'Oracle', color: "1" },
            { x: 5, y: 'Sybase', color: "1" },
            { x: 1, y: 'MySQL', color: "1" },
            { x: 1, y: 'Sqlite', color: "1" },
        ];

        const toolsData = [
            { x: 9, y: 'Visual Studio', color: "1" },
            { x: 9, y: 'Unity3D', color: "1" },
            { x: 1, y: 'Wireshark', color: "1" },
            { x: 1, y: 'Eclipse', color: "1" },
            { x: 6, y: 'Notepad++', color: "1" },
            { x: 7, y: 'Word/Excel', color: "1" },
        ];

        const vcsData = [
            { x: 5, y: 'Bitbucket (Git)', color: "1" },
            { x: 5, y: 'Tortoise SVN', color: "1" },
            { x: 7, y: 'TFS', color: "1" },
        ];

        const networkData = [
            { x: 2, y: 'CCNA (Certified)', color: "1" },
            { x: 2, y: 'CNNP (Certified)', color: "1" },
        ];

        const osData = [
            { x: 20, y: 'Windows', color: "1" },
            { x: 4, y: 'Linux', color: "1" },
        ];

        const miscData = [
            { x: 7, y: 'XML', color: "1" },
            { x: 5, y: 'XAML', color: "1" },
            { x: 7, y: 'JSON', color: "1" },
            { x: 2, y: 'Direct X/Draw/3D', color: "1" },
            { x: 2, y: 'OpenGL', color: "1" },
            { x: 7, y: 'REST', color: "1" },
            { x: 4, y: 'SOAP', color: "1" },
            { x: 1, y: 'SignalR', color: "1" },
        ];

        const chatData = [
            { x: 15, y: 'Skype', color: "1" },
            { x: 10, y: 'Outlook', color: "1" },
            { x: 5, y: 'IRC', color: "1" },
            { x: 2, y: 'Trello', color: "1" },
            { x: 1, y: 'Slack', color: "1" },
            { x: 1, y: 'Discord', color: "1" },
            { x: 6, y: 'Microsoft Teams', color: "1" },
        ];

        return (
            <div>

                <div className="mainContent bg-light shadow p-3 mb-5 rounded">
                    <h2 className="roboto-heavy-black">About</h2>
                    <p>Over the years you come into contact with <b>A LOT</b> of different technologies. Many of which you only spend a few hours on, others... years. So which ones are relevant to show here exactly? Should I only put actual work experience here? Or will my thousands of hours of hobby programming also count as experience? Does education count? I will try to add experience I believe may be useful for others to know. So this list will be an incomplete approximation of my technical experience and knowledge.</p>
                    <p>Updated and relevant as of 2021-01-10.</p>
                </div>

                <div className="mainContent bg-light shadow p-3 mb-5 rounded">
                    <h2 className="roboto-heavy-black">Languages</h2>
                    <ExperienceBarGraph data={languageData} margin={120}/>
                </div>

                <div className="mainContent bg-light shadow p-3 mb-5 rounded">
                    <h2 className="roboto-heavy-black">Frameworks</h2>
                    <ExperienceBarGraph data={frameworkData} margin={120} />
                </div>

                <div className="mainContent bg-light shadow p-3 mb-5 rounded">
                    <h2 className="roboto-heavy-black">Databases</h2>
                    <ExperienceBarGraph data={databaseData} margin={120} />
                </div>

                <div className="mainContent bg-light shadow p-3 mb-5 rounded">
                    <h2 className="roboto-heavy-black">Tools / Editors</h2>
                    <ExperienceBarGraph data={toolsData} margin={120} />
                </div>

                <div className="mainContent bg-light shadow p-3 mb-5 rounded">
                    <h2 className="roboto-heavy-black">Version Control Systems</h2>
                    <ExperienceBarGraph data={vcsData} margin={120} />
                </div>

                <div className="mainContent bg-light shadow p-3 mb-5 rounded">
                    <h2 className="roboto-heavy-black">Computer Networks</h2>
                    <ExperienceBarGraph data={networkData} margin={120} />
                </div>

                <div className="mainContent bg-light shadow p-3 mb-5 rounded">
                    <h2 className="roboto-heavy-black">Operating Systems</h2>
                    <ExperienceBarGraph data={osData} margin={120} />
                </div>

                <div className="mainContent bg-light shadow p-3 mb-5 rounded">
                    <h2 className="roboto-heavy-black">Communication</h2>
                    <ExperienceBarGraph data={chatData} margin={120} />
                </div>

                <div className="mainContent bg-light shadow p-3 mb-5 rounded">
                    <h2 className="roboto-heavy-black">Misc</h2>
                    <ExperienceBarGraph data={miscData} margin={120} />
                </div>
            </div>
        );
    }


}
