﻿import React, { Component } from 'react';
import { DropdownItem } from 'reactstrap';
import { Link } from 'react-router-dom';

export class DropdownAppLink extends Component {
    static displayName = DropdownAppLink.name;

    constructor(props) {
        super(props);

        this.state = {
            text: props.text,
            link: props.link,
            badge1: props.badge1,
            badge2: props.badge2,
            badge3: props.badge3
        };
    }


    renderBadge(badge) {

        let text = ''
        if (badge.length > 0) {
            text = <kbd className="badge mr-1">{badge}</kbd>;
        }
        return text;
    }

    render() {

        return (

            <DropdownItem tag={Link} to={this.state.link}>
                <div>
                    <div className="d-inline-flex pl-0 pt-0">{this.state.text}</div>
                    <div>
                        {this.renderBadge(this.state.badge1)}
                        {this.renderBadge(this.state.badge2)}
                        {this.renderBadge(this.state.badge3)}
                    </div>
                </div>
            </DropdownItem>

        );
    }
}
export default DropdownAppLink;
