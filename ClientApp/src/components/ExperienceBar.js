﻿import React, { Component } from 'react';

export class ExperienceBar extends Component {
    static displayName = ExperienceBar.name;

    constructor(props) {
        super(props);

        this.state = {
            headline: props.headline,
            workExp: props.workExp,
            schoolExp: props.schoolExp,
            hobbyExp: props.hobbyExp
        };
    }

    renderWorkExperience() {

        let text = ''
        if (this.state.workExp.length > 0) {
            text = <kbd className="badge workSchoolHobbyExperienceColor mr-2"><i className="fa fa-briefcase" aria-hidden="true" /> {this.state.workExp}</kbd>;
        }
        return text;
    }

    renderSchoolExperience() {

        let text = ''
        if (this.state.schoolExp.length > 0) {
            text = <kbd className="badge workSchoolHobbyExperienceColor mr-2"><i className="fa fa-graduation-cap" aria-hidden="true" /> {this.state.schoolExp}</kbd>;
        }
        return text;
    }

    renderHobbyExperience() {

        let text = ''
        if (this.state.hobbyExp.length > 0) {
            text = <kbd className="badge workSchoolHobbyExperienceColor mr-2"><i className="fa fa-heart" aria-hidden="true" /> {this.state.hobbyExp}</kbd>;
        }
        return text;
    }
    render() {

        return (

            <div className="mt-4">
                <div className="d-inline-flex pl-0 pt-2"><kbd className="badge-primary">{this.state.headline}</kbd></div>
                <div>
                    {this.renderWorkExperience()}
                    {this.renderHobbyExperience()}
                    {this.renderSchoolExperience()}
                </div>
            </div>

        );
    }


}
