﻿import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export class Current extends Component {
    static displayName = Current.name;

    constructor(props) {
        super(props);

        this.state = {

        };
    }


    render() {

        return (

            <div>
                <div className="mainContent bg-light shadow p-3 mb-5 rounded">

                    <div className="d-flex flex-wrap">

                        <div>
                            <h2 className="roboto-heavy-black mb-0">Current project</h2>
                            <div className="d-inline-flex">
                                <kbd className="badge mr-1">C#</kbd>
                                <kbd className="badge mr-1">Unity3D</kbd>
                                <kbd className="badge mr-1">Shader programming</kbd>
                                <kbd className="badge mr-1">Noise</kbd>
                                <kbd className="badge mr-1">Marching Cubes</kbd>
                                <kbd className="badge mr-1">Threading</kbd>
                                <kbd className="badge mr-1">A* Pathfinding</kbd>
                            </div>

                            <h5 className="roboto-heavy-black pt-4">Code</h5>
                            <div className="d-inline-flex">
                                (Not available)
                            </div>

                            <h5 className="roboto-heavy-black pt-4">Description</h5>
                            <div className="d-inline-flex">
                                <p className="roboto-normal-black">
                                    I have for a few years now been working on a massive project that I honestly have no idea if I can finish. 
                                    It is basically a 3D game and endless in the same way as Minecraft is. The game is supposed to be more like an endless Diablo game. 
                                    I have spend months (probably years) learning shader programming, pathfinding algorithms and other algorithms/techniques to tackle various problems.
                                    The game is heavily threaded because it needs to do a lot of noise and mesh calculations in the background.
                                </p>
                            </div>

                            <h5 className="roboto-heavy-black pt-4">Media</h5>
                        </div>
                    </div>

                    <img src="/img/fstback.png" className="img-fluid" />

                </div>
            </div>

        );
    }


}
