﻿import React, { Component } from 'react';
import moment from 'moment'

export class Comment extends Component {
    static displayName = Comment.name;

    constructor(props) {
        super(props);

        this.state = {
            id: props.id,
            name: props.name,
            text: props.text,
            datetimeRaw: props.datetime,
            datetime: moment(props.datetime).format('YYYY-MM-DD HH:mm:ss'),
            edited: props.edited,
            onDelete: props.onDelete,
            onUpdate: props.onUpdate,
            toggleEdit: false
        };

        this.handleCommentDelete = this.handleCommentDelete.bind(this);
        this.toogleCommentEdit = this.toogleCommentEdit.bind(this);
        this.saveCommentEdit = this.saveCommentEdit.bind(this);
    }

    renderCommentText() {

        let commentText = '';
        if (this.state.toggleEdit) {
            commentText = (
                <div>
                    <form onSubmit={this.saveCommentEdit} className="needs-validation" novalidate>
                        <textarea className="form-control" id="commentEditTextArea" rows="10" placeholder={this.state.text}>{this.state.text}</textarea>
                        <div className="btn-group" role="group">
                            <button type="submit" className="btn btn-success">Update comment</button>
                            <button type="button" className="btn btn-danger" onClick={this.toogleCommentEdit}>Cancel</button>
                        </div>
                    </form>
                </div>
            );
        }
        else {
            commentText = <p className="card-text">{this.state.text}</p>;
        }

        return commentText;
    }

    renderCommentEdited() {

        let editedText = '';
        if (this.state.edited) {
            editedText = "(Edited)";
        }
        return editedText;
    }

    render() {

        return (
            <div className="card text-dark bg-light mb-3">

                <div className="card-header">

                    <button type="button" className="btn btn-link pull-right" onClick={this.handleCommentDelete}>
                        <i className="fa fa-times" aria-hidden="true"></i>
                    </button>

                    <button type="button" id="editButton" className="btn btn-link pull-right" onClick={this.toogleCommentEdit}>
                        <i className="fa fa-pencil" aria-hidden="true"></i>
                    </button>

                    <table>
                        <tr><b>{this.state.name} {this.renderCommentEdited()}</b></tr>
                        <tr><small>{this.state.datetime}</small></tr>
                    </table>

                </div>

                <div className="card-body">
                    {this.renderCommentText()}
                </div>

            </div>
        );
    }

    // Callback to parent to delete self
    handleCommentDelete()  {
        this.state.onDelete(this.state.id);
    }

    toogleCommentEdit() {

        this.setState({ toggleEdit: !this.state.toggleEdit });
    }

    saveCommentEdit = (event) => {

        // Prevent page reload
        event.preventDefault();

        // Update
        const commentText = event.target.elements['commentEditTextArea'].value;
        if (commentText.length > 0) {
            if (this.state.onUpdate(this.state.id, commentText, this.state.name, this.state.datetimeRaw)) {
                this.setState({ text: commentText, toggleEdit: false, edited: 1 });
            }
        }
        else {
            alert("You have to write something...");
        }
    }
    
}
