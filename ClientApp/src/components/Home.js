import React, { Component } from 'react';

export class Home extends Component {
  static displayName = Home.name;

  render () {
    return (
      <div>
            <h2>Welcome.</h2>
            <p>This website runs on a Raspberry Pi 4 with Linux (Ubuntu) as operating system.</p>
            <p>The site is built with C# ASP.NET Core as backend and React as frontend.</p>
      </div>
    );
  }
}
