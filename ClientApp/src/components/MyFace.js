﻿import React, { Component } from 'react';

export class MyFace extends Component {
    static displayName = MyFace.name;

    constructor(props) {
        super(props);

        this.state = {
        };
    }

    render() {

        return (

            <div className="navbar-left d-flex p-2">
                <img src="/img/circle_frame.png" className="img-fluid" />
                <div className="align-self-center h-roboto-heavy-italic">
                    <h3 className="m-0">Fredrik</h3>
                    <h3 className="m-0">Strömbergsson</h3>
                </div>
            </div>

        );
    }


}
