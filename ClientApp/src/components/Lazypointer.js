﻿import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export class Lazypointer extends Component {
    static displayName = Lazypointer.name;

    constructor(props) {
        super(props);

        this.state = {

        };
    }


    render() {

        return (

            <div>
                <div className="mainContent bg-light shadow p-3 mb-5 rounded">

                    <div className="d-flex flex-wrap">

                        <div>
                            <h2 className="roboto-heavy-black mb-0">Lazy Pointer</h2>
                            <div className="d-inline-flex">
                                <kbd className="badge mr-1">Java</kbd>
                                <kbd className="badge mr-1">2D Platformer</kbd>
                                <kbd className="badge mr-1">OpenGL</kbd>
                                <kbd className="badge mr-1">LibGDX Framework</kbd>
                            </div>

                            <h5 className="roboto-heavy-black pt-4">Code</h5>
                            <div className="d-inline-flex">
                                (Not available)
                            </div>

                            <h5 className="roboto-heavy-black pt-4">Description</h5>
                            <div className="d-inline-flex">
                                <p className="roboto-normal-black">
                                    The game mechanics are quite simple, you klick to walk and try to avoid all the bad stuff while moving towards the end of the level.
                                    There are seven levels in this game and they were all designed in the <Link to="/apps/lazypointer-editor">level editor</Link> I made specifically for this game. Building this editor made the process of making new levels very fast and easy.
                                    Looking back at this code now, its not very impressive... but I learned a lot by making this. And it was fun!
                                </p>
                            </div>

                            <h5 className="roboto-heavy-black pt-4">Media</h5>
                        </div>
                    </div>

                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe className="embed-responsive-item" src="https://www.youtube.com/embed/hyFGhUbiDo0" frameborder="0" gesture="media" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    </div>

                </div>
            </div>

        );
    }


}
