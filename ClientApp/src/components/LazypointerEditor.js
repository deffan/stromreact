﻿import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export class LazypointerEditor extends Component {
    static displayName = LazypointerEditor.name;

    constructor(props) {
        super(props);

        this.state = {

        };
    }


    render() {

        return (

            <div>
                <div className="mainContent bg-light shadow p-3 mb-5 rounded">

                    <div className="d-flex flex-wrap">

                        <div>
                            <h2 className="roboto-heavy-black mb-0">Lazy Pointer Level Editor</h2>
                            <div className="d-inline-flex">
                                <kbd className="badge mr-1">Java</kbd>
                            </div>

                            <h5 className="roboto-heavy-black pt-4">Code</h5>
                            <div className="d-inline-flex">
                                (Not available)
                            </div>

                            <h5 className="roboto-heavy-black pt-4">Description</h5>
                            <div className="d-inline-flex">
                                <p className="roboto-normal-black">
                                    This is a level editor I made for my game <Link to="/games/lazypointer">Lazy Pointer</Link> which made it much easier and faster to build levels. 
                                    Before I had this editor all levels were basically written by hand in text-files. Which is not very effective, easy or fun. This greatly improved things.
                                </p>
                            </div>

                            <h5 className="roboto-heavy-black pt-4">Media</h5>
                        </div>
                    </div>

                    <img src="/img/lazypointereditor.png" className="img-fluid" />

                </div>
            </div>

        );
    }


}
