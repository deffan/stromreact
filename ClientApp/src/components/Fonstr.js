﻿import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export class Fonstr extends Component {
    static displayName = Fonstr.name;

    constructor(props) {
        super(props);

        this.state = {

        };
    }


    render() {

        return (

            <div>
                <div className="mainContent bg-light shadow p-3 mb-5 rounded">

                    <div className="d-flex flex-wrap">

                        <div>
                            <h2 className="roboto-heavy-black mb-0">Fonstr</h2>
                            <div className="d-inline-flex">
                                <kbd className="badge mr-1">C#</kbd>
                                <kbd className="badge mr-1">WPF</kbd>
                                <kbd className="badge mr-1">REST</kbd>
                                <kbd className="badge mr-1">API</kbd>
                                <kbd className="badge mr-1">IRC</kbd>
                            </div>

                            <h5 className="roboto-heavy-black pt-4">Code</h5>
                            <div className="d-inline-flex">
                                <a href="https://bitbucket.org/deffan/fonstr/src/master/">
                                    <img src="/img/Bitbucket@2x-blue.png" className="bitBucketLink" />
                                </a>
                            </div>

                            <h5 className="roboto-heavy-black pt-4">Description</h5>
                            <div className="d-inline-flex">
                                <p className="roboto-normal-black">
                                    "Fonstr" is supposed to be a shorter version of "Fönster" which means "Windows" in Swedish. Yes, the imagination is massive with this one!
                                    After making only games as hobby projects I decided I had to make a desktop application. So I decided to make one using C# and WPF as front-end framework.
                                    The idea of this application is to be a tool and editor for Twitch steamers. The application is able to listen to events, such as Keyboard input or listen to chat messages on IRC.
                                    Once an event has occured, you can match it against rules that you set, and then execute a command. The command is then sent to a window that you create in the editor.
                                    Each window can have one or many components. And each component have a function, such as playing an animation, playing a sound, perhaps writing text or transforming (moving, scaling, rotating) an object.
                                    The application ended up being quite complex actually.
                                </p>
                            </div>

                        </div>
                    </div>

                    <h5 className="roboto-heavy-black pt-4">This is a screenshot of the Editor part of the application</h5>
                    <img src="/img/fonstr_screen_01.png" className="img-fluid" />

                    <h5 className="roboto-heavy-black pt-4">This is a screenshot of the Control/Settings part of the application</h5>
                    <img src="/img/fonstr_screen_02.png" className="img-fluid" />

                    <h5 className="roboto-heavy-black pt-4">Here is a video showing a real example of an animation made in Fonstr</h5>
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe className="embed-responsive-item" src="https://www.youtube.com/embed/bWtpbSkgHVs" frameborder="0" gesture="media" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    </div>
                </div>
            </div>

        );
    }


}
