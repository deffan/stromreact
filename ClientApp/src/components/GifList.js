import React, { Component } from 'react';
import { Gif } from './Gif';

const endpoint = 'api/gif/';

export class GifList extends Component {
    static displayName = Gif.name;

    constructor(props) {
    super(props);
        this.state = {
            loading: false,
            gifs: []
        };

        this.handleSearch = this.handleSearch.bind(this);
    }

    renderGifs(gifs) {
        return (
            <div className="row justify-content-start">
                {gifs.map(item => <Gif key={item.id} id={item.id} title={item.title} url={item.url} preview={item.previewUrl} />)}
            </div>
        );
    }

    renderSearch() {
        return (
            <div>
                <div className="bg-light shadow p-3 mb-5 rounded">
                    <h1><i className="fa fa-file-image-o" aria-hidden="true"></i> Gif's</h1>
                    <p>This page demonstrates searching and presenting Gif's from <a href="https://giphy.com/">Giphy's API.</a></p>
                </div>

                <form onSubmit={this.handleSearch}>
                    <div className="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Search..." id="searchField"></input>
                        <button className="btn btn-outline-secondary" type="submit" value="Submit" id="searchButton">Search</button>
                    </div>
                </form>
            </div>
        );
    }

    render() {

        return (
            <div>
                {this.renderSearch()}
                {this.renderGifs(this.state.gifs)}
            </div>
        );

    }

    async GetGifData(searchString) {

        const response = await fetch(endpoint + "search/" + searchString);

        if (response.ok) {
            const data = await response.json();
            this.setState({ gifs: data });
        }
        else {
            alert("HTTP-Error: " + response.status);
        }
        this.setState({ loading: false });
    }

    handleSearch = (event) => {
        let searchString = event.target.elements['searchField'].value;
        this.setState({ loading: true });
        event.preventDefault();
        event.target.reset();
        this.GetGifData(searchString);
    }
}

