﻿import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export class Website extends Component {
    static displayName = Website.name;

    constructor(props) {
        super(props);

        this.state = {

        };
    }


    render() {

        return (

            <div>
                <div className="mainContent bg-light shadow p-3 mb-5 rounded">

                    <div className="d-flex flex-wrap">

                        <div>
                            <h2 className="roboto-heavy-black mb-0">This website</h2>
                            <div className="d-inline-flex">
                                <kbd className="badge mr-1">C#.NET Core</kbd>
                                <kbd className="badge mr-1">React</kbd>
                                <kbd className="badge mr-1">Rasperry PI 4</kbd>
                                <kbd className="badge mr-1">Ubuntu</kbd>
                            </div>

                            <h5 className="roboto-heavy-black pt-4">Code</h5>
                            <div className="d-inline-flex">
                                <a href="https://bitbucket.org/deffan/stromreact/src/master/">
                                    <img src="/img/Bitbucket@2x-blue.png" className="bitBucketLink" />
                                </a>
                            </div>

                            <h5 className="roboto-heavy-black pt-4">Description</h5>
                            <div className="d-inline-flex">
                                <p className="roboto-normal-black">
                                    Previously I had a website made in PHP and it was very simplistic. I decided it was time to learn React so I rebuilt my website in that. 
                                    I chose to use .NET Core as backend and decided to host it on my Raspberry PI due to that my current webhosting service would not support anything but PHP.
                                </p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        );
    }


}
