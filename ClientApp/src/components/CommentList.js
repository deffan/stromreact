﻿import React, { Component } from 'react';
import { Comment } from './Comment';

const endpoint = 'api/comment/';

export class CommentList extends Component {

    static displayName = CommentList.name;

    constructor(props) {
        super(props);
        this.state = {
            comments: [],
            loading: true,
            name: '',
            text: '',
            deleteMeNow: '',
            currentPage: 0,
            totalPageCount: 0
        };

        this.mySubmitHandler = this.mySubmitHandler.bind(this);
        this.nameTextChangeHandler = this.nameTextChangeHandler.bind(this);
        this.commentTextChangeHandler = this.commentTextChangeHandler.bind(this);
        this.fetchNext = this.fetchNext.bind(this);
        this.fetchPrevious = this.fetchPrevious.bind(this);
        this.fetchTotalPageCount = this.fetchTotalPageCount.bind(this);
    }

    componentDidMount() {
        this.fetchTotalPageCount();
        this.GetCommentData(0);
    }

    renderComments(comments) {
        return (
            <div>
                {comments.map(item => <Comment key={item.id} id={item.id} name={item.userName} text={item.text} datetime={item.dateTime} edited={item.edited} onDelete={this.handleCommentDelete} onUpdate={this.handleCommentUpdate} />)}
            </div>
        );
    }

    renderCommentForm() {
        return (
            <div>
                <form onSubmit={this.mySubmitHandler} className="needs-validation" novalidate>

                    <div className="mb-3">
                        <label for="nameInputField" className="form-label">Name</label>
                        <input type="text" className="form-control" id="nameInputField" placeholder="Your name" onChange={this.nameTextChangeHandler} required></input>
                    </div>

                    <div className="mb-3">
                        <label for="commentTextArea" className="form-label">Comment</label>
                        <textarea className="form-control" id="commentTextArea" rows="10" placeholder="Write your comment here..." onChange={this.commentTextChangeHandler} required></textarea>
                    </div>

                    <input className="btn btn-primary" type="submit" id="submitCommentButton" value="Submit"></input>

                </form>
            </div>
        );
    }

    renderPrevForwardButtons() {
        return (
            <div>
                <p>Displaying page {this.state.currentPage + 1} out of {this.state.totalPageCount}</p>
                <button className="btn btn-primary" type="submit" id="prevPageButton" onClick={this.fetchPrevious}>Previous</button>
                <button className="btn btn-primary" type="submit" id="nextPageButton" onClick={this.fetchNext}>Next</button>
            </div>
        );
    }

    render() {

        // Render comments
        let renderComments = this.state.loading ? <p><em>Loading...</em></p> : this.renderComments(this.state.comments);

        // Render
        return (
            <div>
                <div className="bg-light shadow p-3 mb-5 rounded">
                    <h1><i className="fa fa-commenting-o" aria-hidden="true"></i> Comments</h1>
                    <p>This page demonstrates CRUD (Create, Read, Update, Delete) and Pagination on comments.</p>
                </div>
                
                {this.renderCommentForm()}
                <br />
                {renderComments}
                <br />
                {this.renderPrevForwardButtons()}
                <br />
                <br />
            </div>
        );
    }

    fetchNext() {

        if (this.state.currentPage + 1 < this.state.totalPageCount) {
            this.GetCommentData(this.state.currentPage + 1);
        }
        else {
            alert("No next");
        }
    }

    fetchPrevious() {

        if (this.state.currentPage - 1 >= 0) {
            this.GetCommentData(this.state.currentPage - 1);
        }
        else {
            alert("No prev");
        }
    }

    fetchTotalPageCount() {
        this.GetTotalPageCount();
    }

    async GetTotalPageCount() {

        const response = await fetch(endpoint + "pagecount");

        if (response.ok) {
            const data = await response.text();
            this.setState({ totalPageCount: parseInt(data, 10) });
        }
        else {
            alert("HTTP-Error: " + response.status);
        }
    }

    async GetCommentData(page) {

        const response = await fetch(endpoint + page);

        if (response.ok) {
            const data = await response.json();
            this.setState({ comments: data, loading: false, currentPage: page });
        }
        else {
            alert("HTTP-Error: " + response.status);
            this.setState({ loading: false });
        }
    }

    async DeleteComment(commentId) {

        const method = {
            method: 'DELETE',
            headers: {
                'Content-type': 'application/json; charset=UTF-8' 
            },
        }

        try {
            let response = await fetch(endpoint + commentId, method);
            if (response.ok) {
                this.GetCommentData(this.state.currentPage);
                this.fetchTotalPageCount();
            }
        } catch (e) {
            console.log(e.message);
        }
    }

    async UpdateComment(commentId, commentText, name, datetime) {

        var data = {
            id: commentId,
            userName: name,
            dateTime: datetime,
            text: commentText
        };

        const method = {
            method: 'PUT',
            headers: {
                'Content-type': 'application/json; charset=UTF-8'
            },
            body: JSON.stringify(data)
        }

        let result = false;
        try {
            let response = await fetch(endpoint, method);
            result = response.ok;
        } catch (e) {
            console.log(e.message);
        }
        return result;
    }

    async CreateComment(name, text) {

        var data = {
            userName: name,
            text: text
        };

        const method = {
            method: 'POST',
            headers: {
                'Content-type': 'application/json; charset=UTF-8'
            },
            body: JSON.stringify(data)
        }

        try {
            let response = await fetch(endpoint, method);
            if (response.ok) {
                this.GetCommentData(this.state.currentPage);
                this.fetchTotalPageCount();
            }
        } catch (e) {
            console.log(e.message);
        }
    }

    nameTextChangeHandler = (event) => {
        this.setState({ name: event.target.value });
    }

    commentTextChangeHandler = (event) => {
        this.setState({ text: event.target.value });
    }

    mySubmitHandler = (event) => {

        // Prevent page reload
        event.preventDefault();

        // Reset form input fields
        event.target.reset();

        this.CreateComment(this.state.name, this.state.text);
    }

    handleCommentDelete = (commentId) => {

        this.DeleteComment(commentId);
    }

    handleCommentUpdate = (commentId, commentText, userName, dateTime) => {

        return this.UpdateComment(commentId, commentText, userName, dateTime);
    }

}
