﻿import React, { Component } from 'react';

export class Hej extends Component {

    constructor(props) {
        super(props);

        this.state = {
            text: props.text
        };
    }

    render() {

        return (
            <p>{this.state.text}</p>
        );
    }
}