﻿import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export class Wordfrag extends Component {
    static displayName = Wordfrag.name;

    constructor(props) {
        super(props);

        this.state = {

        };
    }


    render() {

        return (

            <div>
                <div className="mainContent bg-light shadow p-3 mb-5 rounded">

                    <div className="d-flex flex-wrap">

                        <div>
                            <h2 className="roboto-heavy-black mb-0">Wordfrag</h2>
                            <div className="d-inline-flex">
                                <kbd className="badge mr-1">C#</kbd>
                                <kbd className="badge mr-1">Unity3D</kbd>
                                <kbd className="badge mr-1">Sqlite</kbd>
                            </div>

                            <h5 className="roboto-heavy-black pt-4">Code</h5>
                            <div className="d-inline-flex">
                                (Not available)
                            </div>

                            <h5 className="roboto-heavy-black pt-4">Description</h5>
                            <div className="d-inline-flex">
                                <p className="roboto-normal-black">
                                    As you might have guessed this is a word game. You drag the mouse over the letters and let go of the mouse and if you made a word, you are given points and the letters disappear.
                                    To figure out if you actually made a (english) word, I used a database (sqlite) with a wordlist of approximately 200000 words. You also had to complete the objectives at the bottom to finish the level.
                                    If I remember correctly there was a couple of manually created levels, and once you finished those, new levels were randomly generated.
                                </p>
                            </div>

                            <h5 className="roboto-heavy-black pt-4">Media</h5>
                        </div>
                    </div>

                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe className="embed-responsive-item" src="https://www.youtube.com/embed/wU_Kq9NDcBM" frameborder="0" gesture="media" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    </div>

                </div>
            </div>

        );
    }


}
