﻿import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export class Greeny extends Component {
    static displayName = Greeny.name;

    constructor(props) {
        super(props);

        this.state = {

        };
    }


    render() {

        return (

            <div>
                <div className="mainContent bg-light shadow p-3 mb-5 rounded">

                    <div className="d-flex flex-wrap">

                        <div>
                            <h2 className="roboto-heavy-black mb-0">Greeny</h2>
                            <div className="d-inline-flex">
                                <kbd className="badge mr-1">C++</kbd>
                                <kbd className="badge mr-1">DirectDraw</kbd>
                                <kbd className="badge mr-1">GDI Graphics</kbd>
                            </div>

                            <h5 className="roboto-heavy-black pt-4">Code</h5>
                            <div className="d-inline-flex">
                                (Not available)
                            </div>

                            <h5 className="roboto-heavy-black pt-4">Description</h5>
                            <div className="d-inline-flex">
                                <p className="roboto-normal-black">
                                    This was the FIRST graphical game I ever made! (games before that were basically text/console games). So as you can see in the Youtube video, its very simple.
                                    You play this little green guy and the goal is to kill all the enemies in the level, which then causes a door to open to the next one. The final level is the final boss.
                                </p>
                            </div>

                            <h5 className="roboto-heavy-black pt-4">Media</h5>
                        </div>
                    </div>

                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe className="embed-responsive-item" src="https://www.youtube.com/embed/GxU8SwtnpRI" frameborder="0" gesture="media" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    </div>

                </div>
            </div>

        );
    }


}
