﻿import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export class Starryblocks extends Component {
    static displayName = Starryblocks.name;

    constructor(props) {
        super(props);

        this.state = {

        };
    }


    render() {

        return (

            <div>
                <div className="mainContent bg-light shadow p-3 mb-5 rounded">

                    <div className="d-flex flex-wrap">

                        <div>
                            <h2 className="roboto-heavy-black mb-0">Starry Blocks</h2>
                            <div className="d-inline-flex">
                                <kbd className="badge mr-1">Java</kbd>
                                <kbd className="badge mr-1">OpenGL</kbd>
                                <kbd className="badge mr-1">LibGDX Framework</kbd>
                            </div>

                            <h5 className="roboto-heavy-black pt-4">Code</h5>
                            <div className="d-inline-flex">
                                (Not available)
                            </div>

                            <h5 className="roboto-heavy-black pt-4">Description</h5>
                            <div className="d-inline-flex">
                                <p className="roboto-normal-black">
                                    My first Java game! The idea of this simple game is to free the stars surrounded by blocks, by placing out more blocks. By putting out 3x blocks of the same color in a row. You remove a value from the numbered blocks each time you do this. Eventually there wont be a number and it can be removed completely.
                                </p>
                            </div>

                            <h5 className="roboto-heavy-black pt-4">Media</h5>
                        </div>
                    </div>

                    <img src="/img/starrypic1.png" className="img-fluid" />

                </div>
            </div>

        );
    }


}
