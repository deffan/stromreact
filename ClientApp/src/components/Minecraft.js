﻿import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export class Minecraft extends Component {
    static displayName = Minecraft.name;

    constructor(props) {
        super(props);

        this.state = {

        };
    }


    render() {

        return (

            <div>
                <div className="mainContent bg-light shadow p-3 mb-5 rounded">

                    <div className="d-flex flex-wrap">

                        <div>
                            <h2 className="roboto-heavy-black mb-0">Minecraft Clone</h2>
                            <div className="d-inline-flex">
                                <kbd className="badge mr-1">C++</kbd>
                                <kbd className="badge mr-1">2D Platformer</kbd>
                            </div>

                            <h5 className="roboto-heavy-black pt-4">Code</h5>
                            <div className="d-inline-flex">
                                (Not available)
                            </div>

                            <h5 className="roboto-heavy-black pt-4">Description</h5>
                            <div className="d-inline-flex">
                                <p className="roboto-normal-black">
                                    I am not really sure this can be classified as a game as it really does not do much more than can be seen in the Youtube video. But I just wanted to test a few things! Calling it a minecraft clone might also be a bit silly!
                                </p>
                            </div>

                            <h5 className="roboto-heavy-black pt-4">Media</h5>
                        </div>
                    </div>

                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe className="embed-responsive-item" src="https://www.youtube.com/embed/qJHpDzp3DCQ" frameborder="0" gesture="media" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    </div>

                </div>
            </div>

        );
    }


}
