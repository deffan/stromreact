import React, { Component } from 'react';
import { Collapse, Container, Navbar, NavbarToggler, NavItem, NavLink, UncontrolledDropdown, DropdownToggle, DropdownItem, DropdownMenu } from 'reactstrap';
import { Link } from 'react-router-dom';
import '../css/NavMenu.css';
import { MyFace } from './MyFace';
import { DropdownAppLink } from './DropdownAppLink';

export class NavMenu extends Component {
  static displayName = NavMenu.name;

  constructor (props) {
    super(props);

    this.toggleNavbar = this.toggleNavbar.bind(this);
    this.state = {
      collapsed: true
    };
  }

  toggleNavbar () {
    this.setState({
      collapsed: !this.state.collapsed
    });
  }

  render () {
      return (
          <header>
              <Navbar className="navbar-expand-sm navbar-toggleable-sm ng-dark bg-light navbar-dark border-bottom box-shadow shadow p-3 mb-3 repeatingBackgroundImage">
                <Container>
                <MyFace />
                <NavbarToggler onClick={this.toggleNavbar} className="mr-2" />
                <Collapse className="d-sm-inline-flex flex-sm-row-reverse" isOpen={!this.state.collapsed} navbar>

                    <ol className="navbar-nav flex-grow navbar-dark">
                        <NavItem>
                            <NavLink tag={Link} className="text-light p-roboto-heavy" to="/">Home</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink tag={Link} className="text-light p-roboto-heavy" to="/about">About</NavLink>
                        </NavItem>

                        <UncontrolledDropdown>
                            <DropdownToggle tag="a" className="nav-link text-light p-roboto-heavy" caret>Game Development</DropdownToggle>
                            <DropdownMenu>
                                <DropdownAppLink text="Boxfer" link="/games/boxfer" badge1="" badge2="" badge3=""/>
                                <DropdownAppLink text="WordFrag" link="/games/wordfrag" badge1="" badge2="" badge3="" />
                                <DropdownAppLink text="Lazy Pointer" link="/games/lazypointer" badge1="" badge2="" badge3="" />
                                <DropdownAppLink text="Starry Blocks" link="/games/starryblocks" badge1="" badge2="" badge3="" />
                                <DropdownAppLink text="Mario Style" link="/games/mario" badge1="" badge2="" badge3="" />
                                <DropdownAppLink text="Minecraft Clone" link="/games/minecraft" badge1="" badge2="" badge3="" />
                                <DropdownAppLink text="Wizard" link="/games/wizard" badge1="" badge2="" badge3="" />
                                <DropdownAppLink text="Greeny" link="/games/greeny" badge1="" badge2="" badge3="" />
                                <DropdownItem divider />
                                <DropdownAppLink text="Current project" link="/games/current" badge1="" badge2="" badge3="" />
                            </DropdownMenu>
                        </UncontrolledDropdown>

                        <UncontrolledDropdown>
                            <DropdownToggle tag="a" className="nav-link text-light p-roboto-heavy" caret>Web Projects</DropdownToggle>
                            <DropdownMenu>
                                <DropdownAppLink text="Strombergsson.com" link="/web/website" badge1="" badge2="" badge3="" />
                            </DropdownMenu>
                        </UncontrolledDropdown>

                        <UncontrolledDropdown>
                            <DropdownToggle tag="a" className="nav-link text-light p-roboto-heavy" caret>Desktop Apps</DropdownToggle>
                            <DropdownMenu>
                                <DropdownAppLink text="Fonstr" link="/apps/fonstr" badge1="" badge2="" badge3="" />
                                <DropdownAppLink text="Lazypointer Editor" link="/apps/lazypointer-editor" badge1="" badge2="" badge3="" />
                            </DropdownMenu>
                        </UncontrolledDropdown>

                    </ol>

                </Collapse>
                </Container>
            </Navbar>
      </header>
    );
  }
}
