﻿import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export class Boxfer extends Component {
    static displayName = Boxfer.name;

    constructor(props) {
        super(props);

        this.state = {

        };
    }


    render() {

        return (

            <div>
                <div className="mainContent bg-light shadow p-3 mb-5 rounded">

                    <div className="d-flex flex-wrap">

                        <div>
                            <h2 className="roboto-heavy-black mb-0">Boxfer</h2>
                            <div className="d-inline-flex">
                                <kbd className="badge mr-1">C#</kbd>
                                <kbd className="badge mr-1">Unity3D</kbd>
                                <kbd className="badge mr-1">Noise</kbd>
                                <kbd className="badge mr-1">Procedural generation</kbd>
                            </div>

                            <h5 className="roboto-heavy-black pt-4">Code</h5>
                            <div className="d-inline-flex">
                                (Not available)
                            </div>

                            <h5 className="roboto-heavy-black pt-4">Description</h5>
                            <div className="d-inline-flex">
                                <p className="roboto-normal-black">
                                    You are a box. By clicking you are adding a force to the box in the direction of the click. Gravity is constantly applied. The goal is to see how far can you can reach before you fail.                            
                                    So, how far can you go then? Well, unfortunately, since this game is procedurally generated and endless... that might be an unfair question.
                                    The level is continuously generated as you travel towards the right. The walls and enemies are placed according to noise values.
                                    There was also a high-score list on the web. But that is no longer available.
                                </p>
                            </div>

                            <h5 className="roboto-heavy-black pt-4">Media</h5>
                        </div>
                    </div>

                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe className="embed-responsive-item" src="https://www.youtube.com/embed/sKidPBX8cqI" frameborder="0" gesture="media" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    </div>

                </div>
            </div>

        );
    }


}
