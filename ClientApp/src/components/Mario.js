﻿import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export class Mario extends Component {
    static displayName = Mario.name;

    constructor(props) {
        super(props);

        this.state = {

        };
    }


    render() {

        return (

            <div>
                <div className="mainContent bg-light shadow p-3 mb-5 rounded">

                    <div className="d-flex flex-wrap">

                        <div>
                            <h2 className="roboto-heavy-black mb-0">Mario Style</h2>
                            <div className="d-inline-flex">
                                <kbd className="badge mr-1">C++</kbd>
                                <kbd className="badge mr-1">2D Platformer</kbd>
                                <kbd className="badge mr-1">DirectDraw</kbd>
                            </div>

                            <h5 className="roboto-heavy-black pt-4">Code</h5>
                            <div className="d-inline-flex">
                                (Not available)
                            </div>

                            <h5 className="roboto-heavy-black pt-4">Description</h5>
                            <div className="d-inline-flex">
                                <p className="roboto-normal-black">
                                    I always wanted to make a game like Super Mario! So when I had the chance I took a course called "Game Development in Windows" at university and decided to make that a reality.
                                    Now obviously this game is not like Mario completely, but the similarity is mostly in the [?] boxes you can hit and get a powerup.
                                    Back in those days I was very happy with the result. Looking back at it now, its pretty simplistic, both in apperance and code. But it was great fun to make this.
                                </p>
                            </div>

                            <h5 className="roboto-heavy-black pt-4">Media</h5>
                        </div>
                    </div>

                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe className="embed-responsive-item" src="https://www.youtube.com/embed/VJsHJeaMZNY" frameborder="0" gesture="media" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    </div>

                </div>
            </div>

        );
    }


}
