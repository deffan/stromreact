﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using StromReact.Models;

namespace StromReact.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CommentController : Controller
    {
        private readonly int m_PageSize;
        private readonly ApplicationDbContext m_Db;
  
        public CommentController(ApplicationDbContext db)
        {
            m_PageSize = 5;
            m_Db = db;
        }

        /// <summary>
        /// Get all comments
        /// </summary>
        [HttpGet]
        public IEnumerable<Comment> Get()
        {
            return m_Db.Comment.OrderByDescending(x => x.Id);
        }

        /// <summary>
        /// Get with pagination
        /// </summary>
        [HttpGet("{page:int}")]
        public IEnumerable<Comment> Get(int? page)
        {
            return Get().Skip(page.Value * m_PageSize).Take(m_PageSize).ToList();
        }

        /// <summary>
        /// Get total pages
        /// </summary>
        [HttpGet("pagecount")]
        public int GetPageCount()
        {
            return (int)Math.Ceiling(m_Db.Comment.Count() / (double)m_PageSize);
        }

        /// <summary>
        /// Update a comment
        /// </summary>
        [HttpPut]
        public async Task<IActionResult> Update(Comment comment)
        {
            try
            {
                comment.Edited = 1;
                m_Db.Comment.Update(comment);
                await m_Db.SaveChangesAsync();
                return NoContent();
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        /// <summary>
        /// Delete comment with id
        /// </summary>
        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Delete(int? id)
        {
            try
            {
                if (id == null)
                {
                    return BadRequest("Id is empty.");
                }

                // Find
                Comment c = m_Db.Comment.Where(p => p.Id.Equals(id)).FirstOrDefault();
                if (c != null)
                {
                    // Remove and save
                    m_Db.Comment.Remove(c);
                    await m_Db.SaveChangesAsync();
                    return NoContent();
                }
                return NotFound("Id was not found.");
            }
            catch(Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        /// <summary>
        /// Add comment
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> Post(Comment newComment)
        {
            try
            {
                if (newComment == null)
                {
                    return BadRequest("Invalid post data.");
                }

                // Add current time and get a new highest comment id
                newComment.DateTime = DateTime.Now;
                newComment.Id = GetNewId();
                newComment.Edited = 0;

                // Add and save
                await m_Db.Comment.AddAsync(newComment);
                await m_Db.SaveChangesAsync();

                return Created("", "");
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        /// <summary>
        /// Get a new comment id
        /// </summary>
        private int GetNewId()
        {
            return m_Db.Comment.Max(p => p.Id) + 1;
        }
    }
}