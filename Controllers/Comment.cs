﻿using System;
using System.ComponentModel.DataAnnotations;

namespace StromReact.Models
{
    public class Comment
    {
        [Key]
        public int Id { get; set; }
        public DateTime DateTime { get; set; }
        public int Edited { get; set; }
        public string UserName { get; set; }
        public string Text { get; set; }
    }
}
