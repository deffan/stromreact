﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using StromReact.Lib;
using StromReact.Models;

namespace StromReact.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class GifController : Controller
    {
        private static string m_ApiKey = "";
        private readonly ApplicationDbContext m_Db;

        public GifController(ApplicationDbContext db)
        {
            m_Db = db;

            // Read in API key from database
            if(string.IsNullOrEmpty(m_ApiKey))
            {
                Api apiKey = m_Db.Api.Where(p => p.Vendor.Equals("GIPHY")).FirstOrDefault();
                m_ApiKey = apiKey?.ApiKey;
            }
        }

        /// <summary>
        /// Search and return gifs
        /// </summary>
        [HttpGet("search/{searchString}")]
        public async Task<IEnumerable<Gif>> Search(string searchString)
        {
            List<Gif> result = new List<Gif>();
            if (string.IsNullOrEmpty(searchString) || searchString.Length < 3 || searchString.Length > 100)
            {
                return result;
            }

            // Preform search via Giphy's api
            string search = $"https://api.giphy.com/v1/gifs/search?api_key={m_ApiKey}&q={searchString}&limit=5&offset=0&rating=g&lang=en";
            HttpWebRequest http = (HttpWebRequest)WebRequest.Create(search);
            using (WebResponse response = await http.GetResponseAsync())
            {
                // Get data
                using Stream s = response.GetResponseStream();
                using StreamReader streamReader = new StreamReader(s);
                string data = await streamReader.ReadToEndAsync();

                // Construct a list of gifs
                GiphyDTO gifData = JsonConvert.DeserializeObject<GiphyDTO>(data);
                if(gifData != null)
                {
                    for(int i = 0; i < gifData.data.Count; i++)
                    {
                        Gif gif = new Gif
                        {
                            Id = gifData.data[i].id,
                            Title = gifData.data[i].title,
                            Url = gifData.data[i].url,
                            PreviewUrl = gifData.data[i]?.images?.fixed_height?.url
                        };

                        // Check a few other attributes if this one didnt exist. This seems a bit random...
                        if (string.IsNullOrEmpty(gif.PreviewUrl))
                        {
                            gif.PreviewUrl = gifData.data[i]?.images?.fixed_width?.url;
                            if (string.IsNullOrEmpty(gif.PreviewUrl))
                            {
                                gif.PreviewUrl = gifData.data[i]?.images?.fixed_height_still?.url;
                            }
                        }
                        result.Add(gif);
                    }
                }
            }

            return result;
        }

    }
}