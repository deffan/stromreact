﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StromReact.Models
{
    public class Gif
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public string PreviewUrl { get; set; }
    }
}
