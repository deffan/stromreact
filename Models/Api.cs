﻿using System;
using System.ComponentModel.DataAnnotations;

namespace StromReact.Models
{
    public class Api
    {
        [Key]
        public string Vendor { get; set; }
        public string ApiKey { get; set; }
    }
}
