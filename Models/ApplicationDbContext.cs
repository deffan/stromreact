﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StromReact.Models
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<Comment> Comment { get; set; }
        public DbSet<Api> Api { get; set; }

        public ApplicationDbContext(DbContextOptions options) : base(options)
        {

        }
    }
}
